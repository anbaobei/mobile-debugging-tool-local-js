# 移动端调试工具本地js
 
   这个是eruda 插件  以下代码 引入即可使用
   <!-- <script type="text/javascript" src="//cdn.bootcss.com/eruda/1.5.2/eruda.min.js"></script> -->
  <!-- <script>eruda.init()</script> -->
  
  
  
  这个是vconsole 插件  以下代码 引入即可使用
   vconsole.min.js  是一个非常好用的  移动端调试工具  通过 navigator.userAgent.toLowerCase()  可以查看接口  以及 设备类型  系统版本
   vconsole.min.js   使用方法
   <script src="./static/js/vconsole.min.js"></script>
  <script>
    var vConsole = new VConsole();     如果需要关闭 vconsoele  请注释改行代码即可
  </script>
  
  
  
  
  
   template 是 一个前端 动态渲染模板插件 
   使用方法如下  
   
   
   
   
   
 <!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <title>template</title>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
</head>
<body>
    <style>
    </style>
    <div class="content">
        数据表
        <input type="text" class="search" style="" placeholder="在此输入表明查询" onkeyup="filterFunction()">
        <div class="box" id="divMap" style="height: 120px; overflow-y: scroll; border: 1px solid #000; padding: 10px 10px; box-sizing: border-box ">
            <ul>
            </ul>
        </div>
    </div>
    <script src="https://www.jq22.com/jquery/jquery-1.7.1.js"></script>
    <script type="text/html" id="testTmps">
    {{each list v i}}
         <li  style="margin: 5px 0" >
                &nbsp &nbsp   <input   type="checkbox" class="type"> &nbsp {{v.name}} <input name="" type="text" value="{{i}}"> &nbsp &nbsp &nbsp &nbsp {{v.name}} <input name="" type="text" value="{{i}}">
         </li>
    {{/each}}
</script>
    <script src="./template.js"></script>
    <script>
        let obj = [
            {
                name: '数据表1'
            },
            {
                name: '数据表2'
            },
            {
                name: '数据表3'
            }, {
                name: '数据表4'

            },
            {
                name: '数据表5'
            },
            {
                name: '数据表6'
            },
            {
                name: '数据表7'
            },
            {
                name: '数据表8'
            },
            {
                name: '数据表9'
            },
            {
                name: '数据表11'
            },
            {
                name: '数据12'
            },
            {
                name: '数据表13'
            },

        ]
        $('.box>ul').html(template('testTmps', { list: obj }))
        function filterFunction() {
            let li = $('li')
            let b = $('.search').val()
            for (i = 0; i < li.length; i++) {
                if (li.eq(i).html().indexOf(b) > 0) {

                    li.eq(i).get(0).style.display = 'block'
                } else {
                    li.eq(i).get(0).style.display = "none";
                }
            }
        }
    </script>
</body>
</html>